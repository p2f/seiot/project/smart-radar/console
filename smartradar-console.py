#!/usr/bin/env python3
# encoding: utf-8

import npyscreen
import collections

bf = collections.deque()

# This application class serves as a wrapper for the initialization of curses
# and also manages the actual forms of the application


class MyTestApp(npyscreen.NPSAppManaged):
    def onStart(self):
        self.registerForm("MAIN", MainForm())


# This form class defines the display that will be presented to the user.


class MainForm(npyscreen.SplitForm):
    def create(self):
        self.i = 1
        self.name = "Test"
        self.keypress_timeout = 10
        self.buf = self.add(npyscreen.TitleBufferPager,
                            name="Serial",
                            editable=False,
                            max_height=5)
        self.chooser = self.add(npyscreen.TitleSelectOne,
                                max_height=4,
                                value=[
                                    1,
                                ],
                                name="Pick One",
                                values=["Option1", "Option2", "Option3"],
                                scroll_exit=True)

    def while_waiting(self):
        bf.append(["test" + str(self.i)])
        self.i += 1
        self.update_list()

    def update_list(self):
        self.buf.buffer(bf.pop(), scroll_end=True, scroll_if_editing=True)
        # self.buf.values = bf
        self.buf.display()

    def afterEditing(self):
        self.parentApp.setNextForm(None)


if __name__ == '__main__':
    TA = MyTestApp()
    TA.run()

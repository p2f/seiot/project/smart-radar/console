# Console

## Install

- create a virtual environment `python -m venv venv && source venv/bin/activate`
- install project dependencies `pip install -r requirements.txt`
- run `./smartradar-console.py`
